"""
二分查询
"""


def rank(key, a):
    lo = 0
    hi = len(a) - 1

    while lo <= hi:
        mid = lo + (hi - lo) // 2
        if key > a[mid]:
            lo = mid + 1
        elif key < a[mid]:
            hi = mid - 1
        else:
            return mid
    else:
        return -1


array = [0, 1, 2, 3, 4, 5, 6, 7, 8]
for i in array:
    print(rank(i, array))
