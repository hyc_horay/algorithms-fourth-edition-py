"""
最大公约数
"""


def gcd(q, p):
    if p == 0:
        return q
    r = q % p
    return gcd(p, r)


print(gcd(11, 10))

